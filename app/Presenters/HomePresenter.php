<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;


final class HomePresenter extends Nette\Application\UI\Presenter
{
    public const DEFAULT_PER_PAGE = 5;

    /**
     * Constructor.
     *
     * @param Nette\Database\Explorer $db
     * @return void
     */
    public function __construct(
        private Nette\Database\Explorer $db
    )
    {
        parent::__construct();
    }

    /**
     * Render default template.
     *
     * @param int $count
     * @param int $page
     * @param string $sort
     * @return void
     */
    public function renderDefault(int $count = self::DEFAULT_PER_PAGE, int $page = 1, string $sort = 'ASC'): void
    {
        if ($count === 0) {
            $count = self::DEFAULT_PER_PAGE;
        }

        if ($page === 0) {
            $page = 1;
        }

        if (empty($sort)) {
            $sort = 'ASC';
        }

        $pagination = $this->preparePagination($count, $page);

        $brands = $this->db
            ->table('brands')
            ->order("name {$sort}")
            ->limit($count, $pagination->limitOffset);

        $this->template->pagination = $pagination;
        $this->template->sort = $sort;
        $this->template->brands = $brands;

        $this->getComponent('brandCreateForm')->setDefaults([
            'brand_name' => '',
        ]);
    }

    /**
     * Create brand form component.
     * 
     * @return Form
     */
    protected function createComponentBrandCreateForm(): Form
    {
        $form = new Form();
        $form->addText('brand_name', 'Název značky')->setRequired();
        $form->addHidden('count');
        $form->addHidden('sort');
        $form->addHidden('page');
        $form->onSuccess[] = [$this, 'brandCreateFormSubmit'];

        return $form;
    }

    /**
     * Handles submitting of create brand form component.
     * 
     * @param array $data
     * @return void
     */
    public function brandCreateFormSubmit(array $data): void
    {
        try {
            $brand = $this->db->table('brands')->insert([
                'name' => $data['brand_name'],
            ]);
        } catch (\Exception $e) {
            $this->flashMessage('Při vytváření nové položky nastala chyba!', 'error');

            return;
        }

        $this->flashMessage("Byla vytvořena položka #{$brand->id}!", 'success');

        $this->redirect('default', [
            'count' => (int) $data['count'],
            'page' => (int) $data['page'],
            'sort' => $data['sort'],
        ]);
    }

    /**
     * Edit brand form component.
     * 
     * @return Form
     */
    protected function createComponentBrandEditForm(): Form
    {
        $form = new Form();
        $form->addHidden('brand_id')->setRequired();
        $form->addText('brand_name', 'Název značky')->setRequired();
        $form->addHidden('count');
        $form->addHidden('sort');
        $form->addHidden('page');
        $form->onSuccess[] = [$this, 'brandEditFormSubmit'];

        return $form;
    }

    /**
     * Handles submitting of edit brand form component.
     * 
     * @param array $data
     * @return void
     */
    public function brandEditFormSubmit(array $data): void
    {
        $brand = $this->db->table('brands')->get((int) $data['brand_id']);
        $brand->update(['name' => $data['brand_name']]);

        $this->flashMessage("Byla upravena položka #{$brand->id}!", 'success');

        $this->redirect('default', [
            'count' => (int) $data['count'],
            'page' => (int) $data['page'],
            'sort' => $data['sort'],
        ]);
    }

    /**
     * Delete brand form component.
     * 
     * @return Form
     */
    protected function createComponentBrandDeleteForm(): Form
    {
        $form = new Form();
        $form->addHidden('brand_id')->setRequired();
        $form->addHidden('count');
        $form->addHidden('sort');
        $form->addHidden('page');
        $form->onSuccess[] = [$this, 'brandDeleteFormSubmit'];

        return $form;
    }

    /**
     * Handles submitting of delete brand form component.
     * 
     * @param array $data
     * @return void
     */
    public function brandDeleteFormSubmit(array $data): void
    {
        $brand = $this->db->table('brands')->get((int) $data['brand_id']);
        $brand->delete();

        $this->flashMessage("Byla smazána položka \"{$brand->name}\"!", 'success');
        
        $this->redirect('default', [
            'count' => (int) $data['count'],
            'page' => (int) $data['page'],
            'sort' => $data['sort'],
        ]);
    }

    /**
     * Prepare data for the pagination.
     *
     * @param int $count
     * @param int $page
     * @return \stdClass
     */
    private function preparePagination(int $count, int $page): \stdClass
    {
        $pagination = new \stdClass();

        $pagination->count = $count;
        $pagination->totalCount = (int) $this->db
            ->query('SELECT count(id) FROM sportisimo.brands')
            ->fetchField(0);

        // In case that there are no signs in db
        if ($pagination->totalCount === 0) {
            $pagination->countOfPages = 0;
            $pagination->page = $page;
            $pagination->limitOffset = 0;

            return $pagination;
        }
        
        $pagination->countOfPages = (int) ceil($pagination->totalCount / $count);

        // In case that pagination was change so page is wrong
        if ($page > $pagination->countOfPages) {
            $page = $pagination->countOfPages;
        }

        $pagination->page = $page;
        $pagination->limitOffset = ($page === 1) ? 0 : (($page * $count) - $count);

        return $pagination;
    }
}
