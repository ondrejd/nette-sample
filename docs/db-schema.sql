#
# Tabulka pro značky
#
CREATE TABLE IF NOT EXISTS brands
(
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NULL,
    CONSTRAINT brands_unique_pk UNIQUE (name)
) COLLATE = utf8mb4_czech_ci;

CREATE INDEX brands_name_index ON brands (name);
