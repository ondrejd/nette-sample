# Testovací úloha

## Spuštění

Příklad je psaný pro Linuxový _bash_, ale jde použít i na Windows s WSL2 a Ubuntu.

```shell
# Stáhneme si projekt
git clone https://gitlab.com/ondrejd/nette-test-task
cd nette-test-task
# Zkopírujeme .env pro docker-compose.yml
cp .env.example .env
# Připravíme a spustíme kontejnery
docker compose build
docker compose up -d
# Nainstalujeme balíčky přes Composer
docker compose exec app /usr/local/bin/composer install
# Naimportujeme schéma databáze
docker exec -i db mysql -u root -ptestuser testpass < docs/db-schema.sql
# Případně i testovací data (volitelné)
docker exec -i db mysql -u root -ptestuser testpass < docs/db-data.sql
```

V posledním příkazu případně nahraďte jméno uživatele (`testuser`) a heslo (`testpass`) za ty, které máte v `.env` souboru.

Aplikace by nyní měla být dostupná přes prohlížeč na adrese `http://localhost`.